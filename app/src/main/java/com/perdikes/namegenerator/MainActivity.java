package com.perdikes.namegenerator;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    private Button generate;
    private Button copy;
    private EditText syllable1;
    private EditText syllable2;
    private EditText syllable3;
    private TextView result;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        generate = findViewById(R.id.generate);
        copy = findViewById(R.id.copy);
        syllable1 = findViewById(R.id.syllable1);
        syllable2 = findViewById(R.id.syllable2);
        syllable3 = findViewById(R.id.syllable3);
        result = findViewById(R.id.result);
        result.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

        generate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                result.setText(composeName());
            }
        });

      copy.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
              ClipData data = ClipData.newPlainText("Generated name", result.getText().toString());
              clipboard.setPrimaryClip(data);

              Toast toast = Toast.makeText(MainActivity.this, "Copied to clipboard", Toast.LENGTH_SHORT);
              toast.setGravity(Gravity.BOTTOM, 0, 50);
              toast.show();
          }
      });

    }

    private String composeName(){
        List<String> firstList = new ArrayList<String>(Arrays.asList(syllable1.getText().toString().split(",")));
        List<String> secondList = new ArrayList<String>(Arrays.asList(syllable2.getText().toString().split(",")));
        List<String> thirdList = new ArrayList<String>(Arrays.asList(syllable3.getText().toString().split(",")));

        return  firstList.get((int) (Math.random()*firstList.size())) +
                secondList.get((int) (Math.random()*secondList.size())) +
                thirdList.get((int) (Math.random()*thirdList.size()));
    }


}
